#!/usr/bin/python

import os
import fnmatch

for root, dir, files in os.walk("git"):
    print root
    print ""
    for items in fnmatch.filter(files, "*.c"):
        print "..." + items
        print ""
