#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#define N 100
#define MAX_AGE 140

void swap(int *xp, int *yp){
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void cocktailsort(int *a, size_t n) {
    while(1) {
        char flag;
        size_t start[2] = {1, n - 1},
               end[2] = {n, 0},
               inc[2] = {1, -1};
        for(int it = 0; it < 2; ++it) {
            flag = 1;
            for(int i = start[it]; i != end[it]; i += inc[it])
                if(a[i - 1] > a[i]) {
                    swap(a + i - 1, a + i);
                    flag = 0;
                }
            if(flag)
                return;
        }                            
    }
}

void shellSort(int intArray[],int n) {
   int inner, outer;
   int valueToInsert;
   int interval = 1;   
   int elements = n;
   int i = 0;
   
   while(interval <= elements/3) {
      interval = interval*3 +1;
   }

   while(interval > 0) {
      
      for(outer = interval; outer < elements; outer++) {
         valueToInsert = intArray[outer];
         inner = outer;
			
         while(inner > interval -1 && intArray[inner - interval] 
            >= valueToInsert) {
            intArray[inner] = intArray[inner - interval];
            inner -=interval;
            printf(" item moved :%d\n",intArray[inner]);
         }
         
         intArray[inner] = valueToInsert;
         printf(" item inserted :%d, at position :%d\n",valueToInsert,inner);
      }
		
      interval = (interval -1) /3;
      i++;
   }          
}

void display(int arr[],int low,int up)
{
	int i;
	for(i=low;i<=up;i++)
		printf("%d ",arr[i]);
}

void quick(int arr[],int low,int up)
{
	int piv,temp,left,right;
	int pivot_placed=0;
	left=low;
	right=up;
	piv=low; /*Take the first element of sublist as piv */

	if(low>=up)
		return;

	/*Loop till pivot is placed at proper place in the sublist*/
	while(pivot_placed==0)
	{
		/*Compare from right to left  */
		while( arr[piv]<=arr[right] && piv!=right )
			right=right-1;
		if( piv==right )
		      pivot_placed=1;
		if( arr[piv] > arr[right] )
		{
			temp=arr[piv];
			arr[piv]=arr[right];
			arr[right]=temp;
			piv=right;
		}
		/*Compare from left to right */
		while( arr[piv]>=arr[left] && left!=piv )
			left=left+1;
		if(piv==left)
			pivot_placed=1;
		if( arr[piv] < arr[left] )
		{
			temp=arr[piv];
			arr[piv]=arr[left];
			arr[left]=temp;
			piv=left;
		}
	}/*End of while */

	printf("-> Pivot Placed is %d -> ",arr[piv]);
	display(arr,low,up);
	printf("\n");

	quick(arr,low,piv-1);
	quick(arr,piv+1,up);
}/*End of quick()*/

               
void bubbleSort(int arr[], int n){
    int i, j;
    bool swapped;
    for (i = 0; i < n-1; i++){
        swapped = false;
        for (j = 0; j < n-i-1; j++){
            if (arr[j] > arr[j+1]){
                swap(&arr[j], &arr[j+1]);
                swapped = true;
            }
        }                                                             
        if (swapped == false)
            break;
     }
}

void selectionSort(int arr[], int n){
    int i, j, min_idx;
    for (i = 0; i < n-1; i++){
        min_idx = i;
        for (j = i+1; j < n; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;
            swap(&arr[min_idx], &arr[i]);
    }
}
 
void insertionSort(int arr[], int n){
    int i, key, j;
    for (i = 1; i < n; i++){
        key = arr[i];
        j = i-1;
        while (j >= 0 && arr[j] > key){
            arr[j+1] = arr[j];
            j = j-1;
        }
        arr[j+1] = key;
    }
}

void merge(int arr[], int l, int m, int r){
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;
    int L[n1], R[n2];
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];
    i = 0; 
    j = 0; 
    k = l;
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){
            arr[k] = L[i];
            i++;
        }
        else{
            arr[k] = R[j];
            j++;
        }
        k++;
    }
    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }
    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int l, int r){
    if (l < r){
        int m = l+(r-l)/2;
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);
    }
}


void heapify(int arr[], int n, int i){
    int largest = i; 
    int l = 2*i + 1;  
    int r = 2*i + 2; 
    if (l < n && arr[l] > arr[largest])
        largest = l;
    if (r < n && arr[r] > arr[largest])
        largest = r;
    if (largest != i){
        swap(&arr[i], &arr[largest]);
        heapify(arr, n, largest);
    }
}
 
void heapSort(int arr[], int n){
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);
    for (int i=n-1; i>=0; i--){
        swap(&arr[0], &arr[i]);
        heapify(arr, i, 0);
    }
}
 
void printArray(int arr[], int size){
    int i;
    for (i=0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

void counting_sort_mm(int *array, int n, int min, int max){
    int i, j, z;
    int range = max - min + 1;
    int *count = malloc(range * sizeof(*array));
           
    for(i = 0; i < range; i++) count[i] = 0;
    for(i = 0; i < n; i++) count[ array[i] - min  ]++;

    for(i = min, z = 0; i <= max; i++) {
        for(j = 0; j < count[i - min]; j++) {
            array[z++] = i; 
        }
    }            
    free(count);
}
 
void counting_sort(int *array, int n){
    int i, min, max;
    min = max = array[0];
    for(i=1; i < n; i++) {
        if ( array[i] < min  ) {
            min = array[i];                  
        } 
        else if ( array[i] > max  ) {
            max = array[i];
        }      
    }
}

int main(int argc,char **argv){
    //Combsort
    int arr[] = {64, 34, 25, 12, 22, 11, 90};
    int n = sizeof(arr)/sizeof(arr[0]);
    bubbleSort(arr, n);
    printf("Sorted array: \n");
    printArray(arr, n);
    int arr1[] = {64, 25, 12, 22, 11};
    n = sizeof(arr1)/sizeof(arr1[0]);
    selectionSort(arr1, n);
    printf("Sorted array: \n");
    printArray(arr1, n);
    int arr2[] = {12, 11, 13, 5, 6};
    n = sizeof(arr2)/sizeof(arr2[0]);
    insertionSort(arr2, n);
    printArray(arr2, n);
    int arr3[] = {12, 11, 13, 5, 6, 7};
    n = sizeof(arr3)/sizeof(arr3[0]);
    mergeSort(arr3, 0, n - 1);
    printf("\nSorted array is \n");
    printArray(arr3, n);
    int array[] = {2,55,77,88,44,99,66,77,11,10};
    n = sizeof(array)/sizeof(array[0]);
    quick(array,0,n-1);
    printf("Sorted list is :\n");
    display(array,0,n-1);
    printf("\n");
    shellSort(array,n);
    int a[] = { 5, -1, 101, -4, 0, 1, 8, 6, 2, 3  };
    size_t l = sizeof(a)/sizeof(a[0]);
    cocktailsort(a, l);
    printArray(arr2, l);
    int ages[N], i;
    for(i=0; i < N; i++) ages[i] = rand()%MAX_AGE;
    counting_sort_mm(ages, N, 0, MAX_AGE);
    for(i=0; i < N; i++) printf("%d ", ages[i]);

    return 0;
}

