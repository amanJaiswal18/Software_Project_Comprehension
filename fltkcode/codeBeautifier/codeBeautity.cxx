#include <FL/Fl.H> //include FLTK library
#include <FL/Fl_Window.H> 
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Multiline_Input.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_File_Chooser.H>
//#include <FL/Fl_Color.H>
#include <stdio.h>
#include <stdlib.h>

Fl_Window *window;
Fl_Multiline_Input *InputEditor;
Fl_Multiline_Output *OutputEditor;
Fl_Button *Copy,*Exit,*Open,*Load;
Fl_File_Chooser *FileChooser;

void BeautifyText(Fl_Widget*){
    long FileSize;
    int Result;
    size_t TotalByteRead;
    char *Buffer;
    FILE *FP = fopen("Input.c","w");
  
    fprintf(FP,"%s",InputEditor->value());
    fclose(FP);
    Result = system("indent -st -bap -bli0 -i4 -l79 -ncs -npcs -npsl -fca -lc79 -fc1 -ts4 Input.c > Output.c");

    FP = fopen("Output.c","r");
    fseek(FP, 0, SEEK_END);
    FileSize = ftell(FP);
    fseek(FP, 0, SEEK_SET);  //same as rewind(f);

    Buffer = (char*)malloc(FileSize + 1);
    TotalByteRead = fread(Buffer, FileSize, 1, FP);
    fclose(FP);
    Buffer[FileSize] = '\0';
    OutputEditor->value(Buffer);
}

void CloseWindow(Fl_Widget*){
    exit(0);
}

void OpenFile(Fl_Widget*){
    FileChooser->show();
}

void LoadFile(Fl_Widget*){
    long FileSize;
    int Result;
    size_t TotalByteRead;
    char *Buffer;
    FILE *FP = fopen(FileChooser->value(),"r");
  
    fseek(FP, 0, SEEK_END);
    FileSize = ftell(FP);
    fseek(FP, 0, SEEK_SET);  //same as rewind(f);

    Buffer = (char*)malloc(FileSize + 1);
    TotalByteRead = fread(Buffer, FileSize, 1, FP);
    fclose(FP);
    Buffer[FileSize] = '\0';
    InputEditor->value(Buffer);
}

int main(int argc, char **argv){
    window = new Fl_Window(2000,1000,"CODE BEAUTIFIER");

    FileChooser = new Fl_File_Chooser("/home/aman/","All Files (*)",1,"Choose File"); 
    FileChooser->type(0);
  
    Open = new Fl_Button(720, 900, 70, 30, "BROWSE");
    Load = new Fl_Button(810, 900, 70, 30, "LOAD");
    InputEditor = new Fl_Multiline_Input(50,50,900,800);
    OutputEditor = new Fl_Multiline_Output(1000, 50, 950, 800);
    Copy = new Fl_Button(900, 900, 70, 30, "Submit");
    Exit = new Fl_Button(990, 900, 70, 30, "Exit");
    
    typedef unsigned int  Fl_Color;  
    
    Exit->color(FL_RED); 
    Copy->color(FL_GREEN); 
    Load->color(FL_YELLOW); 
   
    Open->callback(OpenFile);
    Open->when(FL_WHEN_CHANGED);

    Load->callback(LoadFile);
    Load->when(FL_WHEN_CHANGED);
  
    Copy->callback(BeautifyText); //set callback function
    Copy->when(FL_WHEN_CHANGED); //callback function is called when this button widget chages
  
    Exit->callback(CloseWindow);
    Exit->when(FL_WHEN_CHANGED);
  
    window->end();
    window->show(argc, argv);//This will show the Fl_Window widget
    return Fl::run(); 
}
